package com.msk.temposervico;

import java.util.Calendar;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;

public class TempoServico extends Activity {
	static final int DATE_DIALOG_ID = 0;
	private Button buttonCalc;
	private Button buttonQQ;
	private Button buttonRetired;
	private CheckBox checkBoxRecordDate;
	private int dateDay;
	private int dateMonth;
	private int dateYear;
	private int efDay;
	private int efMonth;
	private int efYear;
	private TextView mDateDisplay;
	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
		public void onDateSet(DatePicker paramAnonymousDatePicker,
				int paramAnonymousInt1, int paramAnonymousInt2,
				int paramAnonymousInt3) {
			mYear = paramAnonymousInt1;
			mMonth = (paramAnonymousInt2 + 1);
			mDay = paramAnonymousInt3;
			updateDisplay();
		}
	};
	private int mDay;
	private int mMonth;
	private Button mPickDate;
	private int mYear;
	private ImageView pic;
	private int qqDay;
	private int qqMonth;
	private int qqYear;
	private EditText recordDay;
	private EditText recordMonth;
	private EditText recordYear;
	private int retDay;
	private int retMonth;
	private int retYear;
	private Spinner spinnerRetired;
	private TableLayout tableTempoAverbado;

	private void addRecordTime() {
		int i = (int) Double.parseDouble(recordYear.getText().toString());
		int j = (int) Double.parseDouble(recordMonth.getText().toString());
		int k = (int) Double.parseDouble(recordDay.getText().toString());
		if (k <= 30)
			;
		while (true) {
			if (j <= 12) {
				efYear = (i + efYear);
				efMonth = (j + efMonth);
				efDay = (k + efDay);
				// return;
				k -= 30;
				j++;
				break;
			}
			j -= 12;
			i++;
		}
	}

	private void calcTime() {
		Calendar localCalendar = Calendar.getInstance();
		dateYear = localCalendar.get(1);
		dateMonth = (1 + localCalendar.get(2));
		dateDay = localCalendar.get(5);
		efYear = (dateYear - mYear);
		efMonth = (dateMonth - mMonth);
		efDay = (dateDay - mDay);
		int i = 0;
		int j = 0;
		if (mDay > dateDay) {
			if (efMonth > 0) {
				efDay = (30 - (mDay - dateDay));
				efMonth = (-1 + efMonth);
			}
		} else {
			if ((mDay > dateDay) && (mMonth == dateMonth)) {
				efDay = (30 - (mDay - dateDay));
				efYear = (-1 + efYear);
				efMonth = 11;
			}
			if (mMonth > dateMonth) {
				efMonth = (12 - (mMonth - dateMonth));
				efYear = (-1 + efYear);
			}
			i = 1 + (dateYear - mYear);
			j = mYear;
		}
		for (int k = 0; k >= i; k++) {

			if (((int) Math.IEEEremainder(mYear, 4.0D) == 0) && (mMonth > 1)) {
				efDay = (-1 + efDay);
				if (mDay > 29)
					efDay = (-1 + efDay);
			}
			if (((int) Math.IEEEremainder(dateYear, 4.0D) == 0)
					&& (dateMonth < 2)) {
				efDay = (-1 + efDay);
				if (mDay < 29)
					efDay = (-1 + efDay);
			}
			addRecordTime();
			recalctime();
			// return;
			efDay = (mDay - dateDay);

			if ((int) Math.IEEEremainder(j, 4.0D) == 0)
				efDay = (1 + efDay);
			j++;
		}
	}

	private void recalctime() {
		if ((efDay > 30) && (efMonth < 12)) {
			efDay = (-30 + efDay);
			efMonth = (1 + efMonth);
		}
		if (efMonth >= 12) {
			if (efDay >= 30) {
				efDay = (-30 + efDay);
				efMonth = (1 + efMonth);
				efMonth = (-12 + efMonth);
				efYear = (1 + efYear);
			}
		} else
			return;
		efMonth = (-12 + efMonth);
		efYear = (1 + efYear);
	}

	private void updateDisplay() {
		mDateDisplay.setText(new StringBuilder().append("Data Inicial: ")
				.append(mDay).append("/").append(mMonth).append("/")
				.append(mYear).append(" "));
	}

	@SuppressWarnings("rawtypes")
	public void CarregaMenuCalc() {
		setContentView(2130903040);
		mDateDisplay = ((TextView) findViewById(2131099652));
		mPickDate = ((Button) findViewById(2131099653));
		buttonCalc = ((Button) findViewById(2131099664));
		buttonQQ = ((Button) findViewById(2131099665));
		buttonRetired = ((Button) findViewById(2131099668));
		recordYear = ((EditText) findViewById(2131099661));
		recordMonth = ((EditText) findViewById(2131099662));
		recordDay = ((EditText) findViewById(2131099663));
		checkBoxRecordDate = ((CheckBox) findViewById(2131099654));
		tableTempoAverbado = ((TableLayout) findViewById(2131099655));
		spinnerRetired = ((Spinner) findViewById(2131099667));
		ArrayAdapter localArrayAdapter = ArrayAdapter.createFromResource(this,
				2131034112, 17367048);
		localArrayAdapter.setDropDownViewResource(17367049);
		spinnerRetired.setAdapter(localArrayAdapter);
		mPickDate.setOnClickListener(new View.OnClickListener() {
			@SuppressWarnings("deprecation")
			public void onClick(View paramAnonymousView) {
				showDialog(0);
			}
		});
		checkBoxRecordDate.setOnClickListener(new View.OnClickListener() {
			public void onClick(View paramAnonymousView) {
				if (((CheckBox) paramAnonymousView).isChecked()) {
					tableTempoAverbado.setVisibility(0);
					return;
				}
				tableTempoAverbado.setVisibility(8);
			}
		});
		buttonQQ.setOnClickListener(new View.OnClickListener() {
			public void onClick(View paramAnonymousView) {
				calcTime();
				addRecordTime();
				recalctime();
				int i = 1 + efYear / 5;
				qqYear = (i * 5);
				qqYear -= 1 + efYear;
				qqMonth = (11 - efMonth);
				qqDay = (30 - efDay);
				AlertDialog.Builder localBuilder = new AlertDialog.Builder(
						TempoServico.this);
				localBuilder.setTitle("Próximo Quinquênio");
				localBuilder.setMessage(qqYear + " anos, " + qqMonth
						+ " meses e " + qqDay + " dias.");
				localBuilder.setNeutralButton(i + " Quinquênios", null);
				localBuilder.show();
			}
		});
		buttonRetired.setOnClickListener(new View.OnClickListener() {
			public void onClick(View paramAnonymousView) {
				calcTime();
				switch (spinnerRetired.getSelectedItemPosition()) {
				default:
				case 0:
				case 1:
				case 2:
				case 3:
				}
				while (true) {
					AlertDialog.Builder localBuilder = new AlertDialog.Builder(
							TempoServico.this);
					localBuilder.setTitle("Tempo para Aposentadoria");
					localBuilder.setMessage(retYear + " anos, " + retMonth
							+ " meses e " + retDay + " dias.");
					localBuilder.setNeutralButton("OK", null);
					localBuilder.show();
					// return;
					retYear = (29 - efYear);
					retMonth = (11 - efMonth);
					retDay = (30 - efDay);
					recalctime();
					// continue;
					retYear = (24 - efYear);
					retMonth = (11 - efMonth);
					retDay = (30 - efDay);
					recalctime();
					// continue;
					addRecordTime();
					recalctime();
					retYear = (29 - efYear);
					retMonth = (11 - efMonth);
					retDay = (30 - efDay);
					recalctime();
					// continue;
					addRecordTime();
					recalctime();
					retYear = (24 - efYear);
					retMonth = (11 - efMonth);
					retDay = (30 - efDay);
					recalctime();
				}
			}
		});
		buttonCalc.setOnClickListener(new View.OnClickListener() {
			public void onClick(View paramAnonymousView) {
				calcTime();
				addRecordTime();
				recalctime();
				int i = efYear / 5;
				AlertDialog.Builder localBuilder = new AlertDialog.Builder(
						TempoServico.this);
				localBuilder.setTitle("Tempo de Efetivo Serviço");
				localBuilder.setMessage(efYear + " anos, " + efMonth
						+ " meses e " + efDay + " dias.");
				localBuilder.setNeutralButton(i + " Quinquênios", null);
				localBuilder.show();
			}
		});
		Calendar localCalendar = Calendar.getInstance();
		mYear = localCalendar.get(1);
		mMonth = (1 + localCalendar.get(2));
		mDay = localCalendar.get(5);
		updateDisplay();
	}

	public void CarregaMenuHelp() {
		setContentView(2130903041);
	}

	public void CarregaMenuPrincipal() {
		setContentView(2130903042);
		pic = ((ImageView) findViewById(2131099675));
		pic.setOnClickListener(new View.OnClickListener() {
			public void onClick(View paramAnonymousView) {
				CarregaMenuCalc();
			}
		});
	}

	public void onCreate(Bundle paramBundle) {
		super.onCreate(paramBundle);
		CarregaMenuPrincipal();
	}

	protected Dialog onCreateDialog(int paramInt) {
		switch (paramInt) {
		default:
			return null;
		case 0:
		}
		return new DatePickerDialog(this, mDateSetListener, mYear, -1 + mMonth,
				mDay);
	}

	public boolean onCreateOptionsMenu(Menu paramMenu) {
		paramMenu.add(0, 1, 0, "Início");
		paramMenu.add(0, 2, 0, "Calcular Tempo");
		paramMenu.add(0, 3, 0, "Creditos");
		paramMenu.add(0, 4, 0, "Sair");
		return super.onCreateOptionsMenu(paramMenu);
	}

	public boolean onOptionsItemSelected(MenuItem paramMenuItem) {
		switch (paramMenuItem.getItemId()) {
		default:
			return super.onOptionsItemSelected(paramMenuItem);
		case 1:
			CarregaMenuPrincipal();
			return true;
		case 2:
			CarregaMenuCalc();
			return true;
		case 3:
			CarregaMenuHelp();
			return true;
		case 4:
		}
		finish();
		return true;
	}
}

/*
 * Location: classes-dex2jar.jar Qualified Name:
 * com.msk.calcworktime.calcworktime JD-Core Version: 0.6.2
 */