package com.msk.temposervico;

import java.util.Calendar;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class Calculadora extends FragmentActivity implements OnClickListener {

	private Button btCalcAposenta;
	private DatePicker dpDefineData;
	private EditText diaAverba, mesAverba, anoAverba, diaFerias, mesFerias,
			anoFerias;
	private Spinner spAposentadoria;
	private TextView dataInicial;

	Calendar c = Calendar.getInstance();
	CalculoTempo novoTempo = new CalculoTempo();

	private int diaEf, mesEf, anoEf, mDia, mMes, mAno, diaQQ, mesQQ, anoQQ,
			diaAposenta, mesAposenta, anoAposenta;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tempo_servico);
		iniciar();
		btCalcAposenta.setOnClickListener(this);

	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void iniciar() {

		// Data de Hoje
		mDia = c.get(Calendar.DAY_OF_MONTH);
		mMes = c.get(Calendar.MONTH);
		mAno = c.get(Calendar.YEAR);

		// Botoes
		btCalcAposenta = (Button) findViewById(R.id.btCalcAposenta);

		// Spinner
		spAposentadoria = (Spinner) findViewById(R.id.spAposentadoria);

		// Ajuste do DatePicker
		dpDefineData = (DatePicker) findViewById(R.id.dpDataInicial);
		
		if (Build.VERSION.SDK_INT > Build.VERSION_CODES.HONEYCOMB) {
			dpDefineData.setCalendarViewShown(false);
	    }
		

		dpDefineData.init(c.get(Calendar.YEAR), c.get(Calendar.MONTH),
				c.get(Calendar.DAY_OF_MONTH), new OnDateChangedListener() {
					@Override
					public void onDateChanged(DatePicker view, int ano,
							int mes, int dia) {
						mDia = dia;
						mMes = mes;
						mAno = ano;
					}
				});

	}

	@Override
	public void onClick(View botao) {

		switch (botao.getId()) {

		case R.id.btCalcAposenta:
			CalculaTempo();
			break;
		}
	}

	private void CalculaQuinquenio() {
		// TODO Auto-generated method stub

	}

	private void CalculaAposentadoria() {
		// TODO Auto-generated method stub

	}

	private void CalculaTempo() {

		int[] tempo = novoTempo.tempo(mDia, mMes, mAno);
		if (tempo[0] < 0)
			Toast.makeText(getApplicationContext(),
					"Tempo futuro não é calculado.", Toast.LENGTH_LONG).show();
		else
			Toast.makeText(
					getApplicationContext(),
					"Ano(s): " + tempo[2] + "\nMes(es): " + tempo[1]
							+ "\nDias: " + tempo[0], Toast.LENGTH_LONG).show();
	}

}
