package com.msk.temposervico;

import java.util.Calendar;

public class CalculoQQ {

	private String[] qq;
	private int diaData, mesData, anoData, mDia, mMes, mAno, erro;

	CalculoTempo novoTempo = new CalculoTempo();

	public String[] quinquenio(int dia, int mes, int ano, int diasAdiconais ) {

		int[] tempo = novoTempo.tempo(dia, mes, ano);
		
		if (tempo[0] < 0) // Mensgem de erro
			qq = new String[]{"erro"};
		else {
			
			// Calculo da Quantidade de Quinquenios
			int qtQQ = tempo[0] / 5;
			String QQ = qtQQ + "";
			
			// Calculo do Proximo Quinquenio
			
			// Vetor com os valores de resposta
			qq = new String[]{ QQ , ""};
		
		}
		
		return qq;
	}
}
